<?php return [
    'plugin' => [
        'name' => 'Dashboard',
        'description' => '',
        'greentrax' => [
            'list_label_user' => 'User',
            'form_select_user' => 'Select User',
            'form_upload_files' => 'Upload Files',
            'form_file_support_type' => 'only supports PDF',
            'permission_tab_title' => 'Permission For upload files for users',
            'first_menu' => 'Upload Files',
            'permission_document_tab_title' => 'Document Root',
            'document_root' => 'Document Root',
            'files' => 'Files',
        ],
    ],
];