<?php namespace Greentrax\Dashboard\Models;

use Model;
use Db;
use BackendAuth;
/**
 * Model
 */
class Users extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var string The database table used by the model.
     */
    public $table = 'greentrax_dashboard_users';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachMany = [
        'upload_files' => 'System\Models\File', 'public' => true, 'delete' => true
    ];

    public function getUserIdOptions() 
    {
        $options = [
            null => 'Select User'
        ];

        // $own = BackendAuth::getUser();

        $users = Db::table('users')->get();

        $users->each(function ($user) use (&$options) {
            $options[$user->id] = $user->email;
        });

        return $options;
    }

   
}
