<?php namespace Greentrax\Dashboard;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Greentrax\Dashboard\Components\Document' => 'documents',
            'Greentrax\Dashboard\Components\User' => 'changePassword'
        ];
    }

    public function registerSettings()
    {
    }
}
