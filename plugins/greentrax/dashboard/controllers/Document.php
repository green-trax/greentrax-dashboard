<?php namespace Greentrax\Dashboard\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;
use Greentrax\Dashboard\Models\Users;

class Document extends Controller
{
    public $implement = [    ];
    
    public $requiredPermissions = [
        'document_root' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Greentrax.Dashboard', 'main-menu-item2', 'side-menu-item');
    }

    public function index() {
        $user = BackendAuth::getUser();

        $fileList = [];

        //$files = Users::where(['user_id' => $user->id])->get();
        $files = Users::where(['user_id' => $user->id])->first();

        if(!empty($files)) {
            foreach ($files->upload_files as $file) {
                $fileList[$file->title] = $file->getPath();
            }
        }

        $this->vars['flies'] = $fileList;

        return $this->makePartial('files');
    }
}
