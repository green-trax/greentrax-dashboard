<?php namespace Greentrax\Dashboard\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Db;

class User extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'uploader_pdf_files' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Greentrax.Dashboard', 'main-menu-item', 'side-menu-item');
    }

    public function getUserEmail($id) 
    {
        return Db::table('users')->where(['id' => $id])->first()->email;
    }
}
