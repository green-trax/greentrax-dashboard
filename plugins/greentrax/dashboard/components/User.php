<?php namespace Greentrax\Dashboard\Components;

use Cms\Classes\ComponentBase;
use Auth;
use RainLab\User\Models\User as frontendUser;
use Flash;

class User extends ComponentBase 
{

    public function componentDetails()
    {
        return [
            'name' => 'Password Reset',
            'description' => 'To reset password'
        ];
    }

    public function onChangePassword()
    {
        $logedInuser = Auth::getUser();

        $user = frontendUser::find($logedInuser->id);
        $user->password = input('password');
        $user->password_confirmation = input('password_confirmation');
        $user->save();

        $data = [
            'login' => $logedInuser->name,
            'password' => input('password')
        ];

        $this->autheticate($data);

        Flash::success('password changed successfully');
       
    }

    private function autheticate($data)
    {
        Auth::authenticate($data, true);
        
    }
}

?>