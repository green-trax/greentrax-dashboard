<?php namespace Greentrax\Dashboard\Components;

use Cms\Classes\ComponentBase;
use RainLab\User\Facades\Auth;
use Greentrax\Dashboard\Models\Users;

class Document extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Document list',
            'description' => 'Show list employeewise Documents'
        ];
    }

    public function getDocuments() 
    {
        $fileList = [];

        $user = Auth::getUser();
        $files = Users::where(['user_id' => $user->id])->first();

        if(!empty($files)) 
        {
            foreach ($files->upload_files as $file) 
            {
                $fileList[] = [
                    'title' => $file->title,
                    'path' => $file->getPath()
                ];
            }
        }

        return $fileList;
    }
}