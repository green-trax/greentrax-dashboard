<?php namespace Greentrax\Dashboard\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateGreentraxDashboardUsers extends Migration
{
    public function up()
    {
        Schema::create('greentrax_dashboard_users', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('greentrax_dashboard_users');
    }
}
