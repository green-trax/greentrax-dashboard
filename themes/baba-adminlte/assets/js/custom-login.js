$(window).on('ajaxErrorMessage', function(event, message){
    // these values are required for mapping the error message
    var loginFailMessage = 'The login field is required.';
    var wrongPasswordMessage = 'A user was found to match all plain text credentials however hashed credential "password" did not match.';
    var notFoundUser = 'A user was not found with the given credentials.';
    // This can be any custom JavaScript you want
    getSetLanguage = $('.showSetLanguage').text();

    if(getSetLanguage == 'de') {
      if (loginFailMessage == message) {
          message = $('.loginRequired').text();
      }else if (wrongPasswordMessage == message) {
          message = $('.wrongPasswordMessage').text();
      }else if(notFoundUser == message) {
          message =  $('.notFoundUser').text();
      }
    }

    $('.error-message').html(`<div class="alert alert-danger" style="text-align:center">${message}</div>`)

    // This will stop the default alert() message
    event.preventDefault();
});